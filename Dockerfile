FROM node:16-alpine as build

WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY package.json /app/package.json
COPY package-lock.json /app/package-lock.json
RUN npm install
RUN npm install -g @angular/cli@15.1.0


COPY . /app

RUN npm run build

EXPOSE 4200

CMD ["ng", "serve"]