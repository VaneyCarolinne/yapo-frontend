import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { response } from './models/response.model';

const Url = 'http://localhost:3000/search_tracks?name=';


@Injectable({
  providedIn: 'root'
})

export class SongsService {

  constructor(private http: HttpClient) { }

  getItunesMusic(artistName: string): Observable<response> {
    return this.http.get<response>(Url+artistName);
  }

}
