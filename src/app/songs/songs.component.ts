import { Component, Input } from '@angular/core';
import { SongsService } from '../songs.service';

export interface Song {
  song: string;
  album: string;
  urlPreview: string;
  release: string;
}

var SONG_DATA: Song[] = [
];

@Component({
  selector: 'app-songs',
  templateUrl: './songs.component.html',
  styleUrls: ['./songs.component.css']
})
export class SongsComponent {

  constructor(private api: SongsService){}

  displayedColumns: string[] = ['song','album','urlPreview','release'];
  columnsToDisplay: string[] = this.displayedColumns.slice();
  data: Song[] = SONG_DATA;
  @Input()
  artistName: string = "radiohead";

  onKey(event: any) {
    this.artistName = event.target.value;
  }

  search() {
    this.api
    .getItunesMusic(this.artistName)
    .subscribe(data =>{
      let songs: Song[] = [];
      data.canciones.forEach(c => {
        let song = {
          song: c['trackName'],
          album: c['collectionName'],
          urlPreview: c['previewUrl'],
          release: c['releaseDate'],
        }
        songs.push(song);
      })
      this.data = songs;
    });
  }
}


